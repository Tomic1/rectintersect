## How to build

Use Visual Studio 2017 to build the RectIntersect solution. This will build:
 
* RectIntersect.exe : The command line tool for reading a recttangle list from a json file and produce a list of all intersections of 2 or more rectangels.
* RectIntersectTests.exe: This is a suite of tests of the rectangle intersection detector. It needs to be run from the .\RectIntersectTests directory to acess the test input data. It can be run wihtout arguments to run all tests. To get a list of supported command line options run with "--help".

Note: I've used Visual Studio 2017 Enterprise edition for developing this, but I expect the Community edition will be able to build it as well.

Alternatively, if you need to use a different framework for building this project you need to do the following:

* Build with a c++14 (or newer) compiler
* Build the contents of RectIntersectLib into a static linking library. Set solution dir as include path.
* Build the contents of RectIntersect into an executable, that links RectIntersectLib. Set solution dir as include path.
* Build the contents of RectIntersectTests into an executable, that links RectIntersectLib. Set solution dir as include path.

There should be no need to link to any external libraries.

## Open source software used

* Catch2 as a header-only unit test framework. Link: https://github.com/catchorg/Catch2
* Json for modern c++ as a header-only json parser. Link: https://github.com/nlohmann/json
