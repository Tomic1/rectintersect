#include "RectIntersectLib/RectIntersect.h"
#include "RectIntersectLib/JsonRectParsing.h"

#include <fstream>
#include <iostream>

using namespace std;
using namespace RectIntersect;

int main(int argc, char** args)
{
	constexpr int maxInputRects = 1000;

	if (argc != 2)
	{
		cout << "Unexpected number of arguments. Usage: RectIntersect.exe <path_to_json>\n";
		return 2;
	}

	try
	{
		ifstream input(args[1]);
		vector<Rect> rects = ReadRectsFromJson(input);

		if (rects.size() > maxInputRects)
		{
			cout << "Encountered " << rects.size() << " rectangles in input. Using only first " << maxInputRects << " for intersection calculation.\n";
			rects.resize(maxInputRects);
		}

		StreamingResultAccumulator result(cout);
		GetIntersections(rects, result);
	}
	catch (exception& e)
	{
		cout << "Encountered an exception while calculating rectangle intersections. Exception details : " << e.what();
		return 1;
	}

	return 0;
}
