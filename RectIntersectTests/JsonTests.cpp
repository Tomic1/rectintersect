#include "RectIntersectLib/JsonRectParsing.h"
#include "RectIntersectTests/oss/catch2/catch.hpp"

#include <fstream>

TEST_CASE("Read valid input", "[Json]")
{
	std::ifstream input("Data/sampleInput.json");
	std::vector<Rect> result = ReadRectsFromJson(input);
	REQUIRE(result.size() == 4);
	CHECK(result[0] == Rect(100, 100, 250, 80));
	CHECK(result[1] == Rect(120, 200, 250, 150));
	CHECK(result[2] == Rect(140, 160, 250, 100));
	CHECK(result[3] == Rect(160, 140, 350, 190));
}

TEST_CASE("Missing json file", "[Json]")
{
	// This file shouldn't exit
	std::ifstream input("Data/Missing.json");
	CHECK_THROWS_WITH(ReadRectsFromJson(input), Catch::Matchers::Contains("[json.exception.parse_error.101]"));
}

TEST_CASE("Malformed json", "[Json]")
{
	// Missing last closing brace
	std::ifstream input1("Data/malformedInput1.json");
	CHECK_THROWS_WITH(ReadRectsFromJson(input1), Catch::Matchers::Contains("[json.exception.parse_error.101]"));

	// File truncated
	std::ifstream input2("Data/malformedInput2.json");
	CHECK_THROWS_WITH(ReadRectsFromJson(input2), Catch::Matchers::Contains("[json.exception.parse_error.101]"));

	// Missing a closing brace in the middle of the json file
	std::ifstream input3("Data/malformedInput3.json");
	CHECK_THROWS_WITH(ReadRectsFromJson(input3), Catch::Matchers::Contains("[json.exception.parse_error.101]"));

	// Extra comma at end of list
	std::ifstream input4("Data/malformedInput4.json");
	CHECK_THROWS_WITH(ReadRectsFromJson(input4), Catch::Matchers::Contains("[json.exception.parse_error.101]"));

	// Missing comma between elements in list
	std::ifstream input5("Data/malformedInput5.json");
	CHECK_THROWS_WITH(ReadRectsFromJson(input5), Catch::Matchers::Contains("[json.exception.parse_error.101]"));
}

TEST_CASE("Unexpected input", "[Json]")
{
	// Missing height element in one rect
	std::ifstream input1("Data/unexpectedInput1.json");
	CHECK_THROWS_WITH(ReadRectsFromJson(input1), Catch::Matchers::Contains("Unexpected number of elements encountered in rect"));

	// Replaced height element in one rect with "h2"
	std::ifstream input2("Data/unexpectedInput2.json");
	CHECK_THROWS_WITH(ReadRectsFromJson(input2), Catch::Matchers::Contains("key 'h' not found"));

	// Root element not called "rects"
	std::ifstream input3("Data/unexpectedInput3.json");
	CHECK_THROWS_WITH(ReadRectsFromJson(input3), Catch::Matchers::Contains("key 'rects' not found"));

	// Having additional element in one rect named "h2"
	std::ifstream input4("Data/unexpectedInput4.json");
	CHECK_THROWS_WITH(ReadRectsFromJson(input4), Catch::Matchers::Contains("Unexpected number of elements encountered in rect"));

	// Having additional root element "other_rects"
	std::ifstream input5("Data/unexpectedInput5.json");
	CHECK_THROWS_WITH(ReadRectsFromJson(input5), Catch::Matchers::Contains("Unexpected number of root elements encountered in json."));

	// Having string input instead of int
	std::ifstream input6("Data/unexpectedInput6.json");
	CHECK_THROWS_WITH(ReadRectsFromJson(input6), Catch::Matchers::Contains("[json.exception.type_error.302]"));

	// Having negative height
	std::ifstream input7("Data/unexpectedInput7.json");
	CHECK_THROWS_WITH(ReadRectsFromJson(input7), Catch::Matchers::Contains("Encountered unexpected width and height while parsing rect."));
	
	// Having negative width
	std::ifstream input8("Data/unexpectedInput8.json");
	CHECK_THROWS_WITH(ReadRectsFromJson(input8), Catch::Matchers::Contains("Encountered unexpected width and height while parsing rect."));
}

TEST_CASE("Large input", "[Json]")
{
	std::ifstream input("Data/10kNoIntersection.json");
	std::vector<Rect> result = ReadRectsFromJson(input);
	REQUIRE(result.size() == 10000);
}