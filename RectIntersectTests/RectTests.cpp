#include "RectIntersectLib/Rect.h"
#include "RectIntersectTests/oss/catch2/catch.hpp"

TEST_CASE("Rectangles without intersections", "[Rect]") {
	Rect a(10, 0, 10, 10);

	// Test case where rectangles have vertical overlap
	Rect b(30, 5, 10, 10);
	REQUIRE_FALSE(a.DoesIntersect(b));
	REQUIRE_FALSE(b.DoesIntersect(a));

	// Test case where rectangles have horizontal overlap
	Rect c(5, 30, 10, 10);
	REQUIRE_FALSE(a.DoesIntersect(c));
	REQUIRE_FALSE(c.DoesIntersect(a));

	// Test case where rectangles have no overlap
	Rect d(30, 30, 10, 10);
	REQUIRE_FALSE(a.DoesIntersect(d));
	REQUIRE_FALSE(d.DoesIntersect(a));

	// Test case where rectangles share horizontal edge
	Rect e(5, 10, 10, 10);
	REQUIRE_FALSE(a.DoesIntersect(e));
	REQUIRE_FALSE(e.DoesIntersect(a));

	// Test case where rectangles share vertical edge
	Rect f(20, 5, 10, 10);
	REQUIRE_FALSE(a.DoesIntersect(f));
	REQUIRE_FALSE(f.DoesIntersect(a));

	// Test case where rectangles share corner
	Rect g(20, 10, 10, 10);
	REQUIRE_FALSE(a.DoesIntersect(g));
	REQUIRE_FALSE(g.DoesIntersect(a));
}

void testRectIntersection(const Rect& a, const Rect& b, const Rect& expectedIntersection)
{
	REQUIRE(a.DoesIntersect(b));
	REQUIRE(b.DoesIntersect(a));

	Rect intersection(a);
	intersection.IntersectWith(b);
	REQUIRE(intersection == expectedIntersection);

	// intersection operations should be commutative, so check reverse order as well.
	intersection = b;
	intersection.IntersectWith(a);
	REQUIRE(intersection == expectedIntersection);
}

TEST_CASE("Rectangles with intersections", "[Rect]") {
	Rect a(0, 0, 10, 10);

	// Check that resulting intersection rect for case where one corner of one rect is contained by the other
	Rect b(5, 5, 10, 10);
	Rect expectedABIntersect(5, 5, 5, 5);
	testRectIntersection(a, b, expectedABIntersect);

	// Check that resulting intersection rect for case where two corners of one rect are contained by the other
	Rect c(2, 2, 15, 5);
	Rect expectedACIntersect(2, 2, 8, 5);
	testRectIntersection(a, c, expectedACIntersect);

	// Check that resulting intersection rect for case where one rect is contained by the other
	Rect d(2, 2, 5, 5);
	testRectIntersection(a, d, d);
}