#include "RectIntersectLib/Detail/SubsetIterator.h"
#include "RectIntersectTests/TestUtil.h"

#include "RectIntersectTests/oss/catch2/catch.hpp"

#include <unordered_set>

using namespace std;

TEST_CASE("Subset count", "[Subset]")
{
	CHECK(CountSubsets(4) == 16);
}

TEST_CASE("Subset elements", "[Subset]")
{
	unordered_set<vector<int>, IntVectorHash> encounteredSubsets;
	vector<int> set{1, 2, 3};

	ForEachSubset(set, [&](vector<int> set)
	{
		encounteredSubsets.insert(move(set));
	});

	CHECK(encounteredSubsets.size() == 8);

	// Ensure that expected subsets are found. Also ensure that the order
	// or elements corresponds to their order in the original set.
	CHECK(encounteredSubsets.find(vector<int>()) != encounteredSubsets.end());
	CHECK(encounteredSubsets.find(vector<int>{1}) != encounteredSubsets.end());
	CHECK(encounteredSubsets.find(vector<int>{2}) != encounteredSubsets.end());
	CHECK(encounteredSubsets.find(vector<int>{3}) != encounteredSubsets.end());
	CHECK(encounteredSubsets.find(vector<int>{1, 2}) != encounteredSubsets.end());
	CHECK(encounteredSubsets.find(vector<int>{1, 3}) != encounteredSubsets.end());
	CHECK(encounteredSubsets.find(vector<int>{2, 3}) != encounteredSubsets.end());
	CHECK(encounteredSubsets.find(vector<int>{1, 2, 3}) != encounteredSubsets.end());
}