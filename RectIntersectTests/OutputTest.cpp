#include "RectIntersectLib/JsonRectParsing.h"
#include "RectIntersectLib/RectIntersect.h"
#include "RectIntersectLib/ResultAccumulator.h"

#include "RectIntersectTests/oss/catch2/catch.hpp"

#include <fstream>
#include <sstream>
#include <unordered_set>

using namespace std;
using namespace RectIntersect;

// Reads all lines from a given stream into a vector of strings
vector<string> ReadAllLines(istream& stream)
{
	string readString;
	vector<string> result;

	while (getline(stream, readString))
	{
		result.emplace_back(move(readString));
	}

	return move(result);
}

TEST_CASE("Test output format", "[Output]") {
	std::ifstream input("Data/sampleInput.json");
	vector<Rect> rects = ReadRectsFromJson(input);
	stringstream resultStream;
	StreamingResultAccumulator result(resultStream);
	GetIntersections(rects, result);

	vector<string> resultLines = ReadAllLines(resultStream);

	std::ifstream expectedOutput("Data/sampleOutput.txt");

	vector<string> expectedLines = ReadAllLines(expectedOutput);

	// No need to check that output matches if number of lines is different
	REQUIRE(resultLines.size() == expectedLines.size());

	// The section of the output before the intersection segment is
	// expected to match exactly
	size_t intersectionStartIndex = 0;
	for (size_t i = 0; i < resultLines.size(); i++)
	{
		CHECK(resultLines[i] == expectedLines[i]);
		if (resultLines[i] == "Intersections")
		{
			intersectionStartIndex = i + 1;
			break;
		}
	}

	// Ensure that we reached the intersection segment
	REQUIRE(intersectionStartIndex != 0);

	// The intersections aren't required to be in any given order, so
	// we add all result intersections to a set, and then make sure that all
	// the expected intersections can be found in the set. This works
	// because intersection rect order is expected to be sorted.
	unordered_set<string> intersections;
	for (size_t i = intersectionStartIndex; i < resultLines.size(); i++)
	{
		intersections.insert(resultLines[i]);
	}

	for (size_t i = intersectionStartIndex; i < expectedLines.size(); i++)
	{
		CHECK(intersections.find(expectedLines[i]) != intersections.end());
	}
}
