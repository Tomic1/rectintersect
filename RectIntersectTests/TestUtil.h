#pragma once

#include <vector>

// To use vectors as map keys a custom hash function must be used, so
// using a simple hash function "like the boost guys did"
// https://stackoverflow.com/questions/2590677/how-do-i-combine-hash-values-in-c0x/2595226#2595226
struct IntVectorHash
{
	size_t operator()(const std::vector<int>& vec) const
	{
		std::hash<int> hasher;
		size_t seed = 0;
		for (int elem : vec)
		{
			seed ^= hasher(elem) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
		}
		return seed;
	}
};