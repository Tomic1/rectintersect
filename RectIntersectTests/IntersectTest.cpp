#include "RectIntersectLib/Detail/SubsetIterator.h"
#include "RectIntersectLib/JsonRectParsing.h"
#include "RectIntersectLib/ResultAccumulator.h"
#include "RectIntersectLib/RectIntersect.h"
#include "RectIntersectTests/TestUtil.h"

#include "RectIntersectTests/oss/catch2/catch.hpp"

#include <numeric>
#include <fstream>
#include <random>
#include <sstream>
#include <unordered_map>

using namespace std;
using namespace RectIntersect;

using IntersectMap = unordered_map<vector<int>, Rect, IntVectorHash>;

constexpr int c_maxInputSize = 1000;

// Trivial implementation that gets all intersections for a given set of rectangles
// which checks all rectangle combinations for intersections. Because it has a complexity
// of O(2^n) it won't work in a reasonable time for larger inputs (e.g. >30 rectangles)
IntersectMap TrivialGetAllIntersections(const vector<Rect>& input)
{
	constexpr unsigned long long maxSubsetsAllowed = 1ull << 30;
	if (CountSubsets(input.size()) > maxSubsetsAllowed)
	{
		throw exception("Can't be used for large inputs due to compute complexity.");
	}

	IntersectMap result;

	// We need a set of all indexes (e.g. set having all values from 0 to size of input).
	vector<int> indexes;
	indexes.resize(input.size());
	iota(indexes.begin(), indexes.end(), 0);

	ForEachSubset(indexes, [&](vector<int> subset)
	{
		// Can't have intersection if subset doesn't have at least 2 elements
		if (subset.size() < 2)
		{
			return;
		}

		Rect intersection = input[subset[0]];
		for (size_t i = 1; i < subset.size(); i++)
		{
			if (intersection.DoesIntersect(input[subset[i]]))
			{
				intersection.IntersectWith(input[subset[i]]);
			}
			else
			{
				// When we encounter the first rect that doesn't intersect we can early-out
				return;
			}
		}

		result.insert({ subset, intersection });
	});

	// Move to avoid slow copy in debug build
	return move(result);
}

// Structure used to specify how to randomly generate rectangles,
// by specifying maximum limits of rectangle dimensions and number
// of rectangles to generate. The rectangle generator will generate
// dimensions in the given range with a uniform distribution.
struct RandomRectGenerationData
{
	// Maximum included dimension for the top side. Minimum is implicitly 0.
	int MaxTop;
	// Maximum included dimension for the left side. Minimum is implicitly 0.
	int MaxLeft;
	// Maximum width. Minimum is implicitly 1.
	int MaxWidth;
	// Maximum height. Minimum is implicitly 1.
	int MaxHeight;
	// Number of rectangles to generate
	int RectangleCount;
};

// Generate a list of rectangles based on the input data using a given seed.
vector<Rect> GenerateRandomRects(const RandomRectGenerationData& data, int seed)
{
	mt19937 gen(seed);
	uniform_int_distribution<> leftDistribution(0, data.MaxLeft);
	uniform_int_distribution<> topDistribution(0, data.MaxTop);
	uniform_int_distribution<> widthDistribution(1, data.MaxWidth);
	uniform_int_distribution<> heightDistribution(1, data.MaxHeight);

	vector<Rect> result;
	for (int i = 0; i < data.RectangleCount; i++)
	{
		result.emplace_back(leftDistribution(gen), topDistribution(gen), widthDistribution(gen), heightDistribution(gen));
	}

	// Move to avoid slow copy in debug
	return move(result);
}

// Test will compare the results of the rectangle intersection algorithm
// used in production with trivial algorithm for a given set of rectangles.
// Since the trivial algorithm has a complexity O(2^n), input size is limited.
void TestIntersectionAlgorithm(const vector<Rect>& rects)
{
	// Using the buffered result accumulator to get the list of rectangle intersections after calculation
	BufferedResultAccumulator result;
	GetIntersections(rects, result);

	IntersectMap expectedIntersections = TrivialGetAllIntersections(rects);

	const vector<BufferedResultAccumulator::Intersection>& resultIntersections = result.GetIntersections();
	// We check size as we're going to search for each resulting intersection in
	// the expected intersection map. If the counts are the same, and we find all of
	// the intersection in the expected intersection map it means that we have the 
	// same set of intersections
	CHECK(resultIntersections.size() == expectedIntersections.size());

	for (auto& intersection : resultIntersections)
	{
		auto expectedIter = expectedIntersections.find(intersection.RectIndexes);
		REQUIRE(expectedIter != expectedIntersections.end());
		CHECK(expectedIter->second == intersection.RectIntersection);
	}
}

// Generates random rectangles and checks algorithm correctness on them
// for a given number of iterations.
void RunRandomizedIntersectionTests(const RandomRectGenerationData& data, int iterations)
{
	// A seed is used to unsure tests are reproducible
	static int seed;

	for (int i = 0; i < iterations; i++)
	{
		vector<Rect> rects = GenerateRandomRects(data, seed++);
		TestIntersectionAlgorithm(rects);
	}
}

TEST_CASE("Randomized input intersection tests", "[Intersection]")
{
	RandomRectGenerationData data;

	// High amount of intersections due to close grouping of rectangles
	data.MaxLeft = 10;
	data.MaxTop = 10;
	data.MaxWidth = 20;
	data.MaxHeight = 20;
	data.RectangleCount = 15;

	RunRandomizedIntersectionTests(data, 10);

	// Medium amount of intersections
	data.MaxLeft = 20;
	data.MaxTop = 20;

	RunRandomizedIntersectionTests(data, 10);

	// Low amount of intersections
	data.MaxLeft = 50;
	data.MaxTop = 50;

	RunRandomizedIntersectionTests(data, 10);
}

TEST_CASE("Worst-case no intersections test", "[Intersection]")
{
	vector<Rect> rects;
	// The worst case when all the rectangles have vertical overlap.
	for (int i = 0; i < c_maxInputSize; i++)
	{
		rects.emplace_back(i, 0, 1, 1);
	}

	BufferedResultAccumulator result;
	GetIntersections(rects, result);

	CHECK(result.GetIntersections().size() == 0);
}

TEST_CASE("Exceeding maximum allowed intersections", "[Intersection]")
{
	// The largest intersection count is when all rectangles overlap. This
	// can be achieved by having all rectangle being the same.
	vector<Rect> rects;
	for (int i = 0; i < c_maxInputSize; i++)
	{
		rects.emplace_back(0, 0, 1, 1);
	}

	BufferedResultAccumulator result;
	CHECK_THROWS_WITH(GetIntersections(rects, result), "Subset count exceeded 2^64.");
}