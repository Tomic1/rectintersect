#include "RectIntersectLib/oss/nlohmann/json.hpp"
#include "RectIntersectLib/Rect.h"

#include <istream>

using nlohmann::json;

// The function below is a hook for the nlohmann json parser, which
// will stop working if the declaration is changed. It's used for
// serializing a Rect to json.
void to_json(json& j, const Rect& rect) {
	j = json{ {"x", rect.GetLeft()}, {"y", rect.GetTop()}, {"w", rect.GetWidth()}, {"h", rect.GetHeight()} };
}

// The function below is a hook for the nlohmann json parser, which
// will stop working if the declaration is changed. It's used for
// parsing a rect from json.
void from_json(const json& j, Rect& rect) {
	if (j.size() != 4)
	{
		throw std::exception("Unexpected number of elements encountered in rect.");
	}

	rect = Rect(j.at("x").get<int>(),
		j.at("y").get<int>(),
		j.at("w").get<int>(),
		j.at("h").get<int>());

	if (rect.GetHeight() <= 0 || rect.GetWidth() <= 0)
	{
		throw std::exception("Encountered unexpected width and height while parsing rect.");
	}
}

std::vector<Rect> ReadRectsFromJson(std::istream& stream)
{
	json j;
	stream >> j;

	if (j.size() != 1)
	{
		throw std::exception("Unexpected number of root elements encountered in json.");
	}

	// move to avoid slow copy in debug
	return std::move(j.at("rects"));
}

void WriteRectsToJson(std::ostream& stream, const std::vector<Rect>& rects)
{
	json j;
	j["rects"] = rects;

	stream << j;
}
