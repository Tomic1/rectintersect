#pragma once

#include <exception>
#include <algorithm>

// Utility class for rectangle manipulation in a 2D plane. Rectangles are
// in a coordinate system where the origin is in the top-left corner.
class Rect
{
public:
	Rect(int x, int y, int width, int height)
		: left(x), top(y), right(x + width), bottom(y + height)
	{
#ifdef _DEBUG
		if (width <= 0 || height <= 0)
		{
			throw std::exception("Attempting to construct rectangle with non-positive width or height.");
		}
#endif
	}

	static Rect MakeFromSides(int left, int top, int right, int bottom)
	{
		return Rect(left, top, right - left, bottom - top);
	}

	Rect() = default;
	Rect(const Rect& other) = default;
	Rect& operator=(const Rect& other) = default;

	int GetLeft() const
	{
		return left;
	}

	int GetTop() const
	{
		return top;
	}

	int GetRight() const
	{
		return right;
	}

	int GetBottom() const
	{
		return bottom;
	}

	int GetWidth() const
	{
		return right - left;
	}

	int GetHeight() const
	{
		return bottom - top;
	}

	bool DoesIntersect(const Rect& other) const
	{
		return GetLeft() < other.GetRight() &&
			GetTop() < other.GetBottom() &&
			other.GetLeft() < GetRight() &&
			other.GetTop() < GetBottom();
	}

	void IntersectWith(const Rect& other)
	{
#ifdef _DEBUG
		if (!DoesIntersect(other))
		{
			throw std::exception("Attempting to get intersection rect for two rectangles that don't intersect.");
		}
#endif

		top = std::max(GetTop(), other.GetTop());
		left = std::max(GetLeft(), other.GetLeft());
		right = std::min(GetRight(), other.GetRight());
		bottom = std::min(GetBottom(), other.GetBottom());
	}

	bool operator==(const Rect& other) const
	{
		return GetLeft() == other.GetLeft() &&
			GetRight() == other.GetRight() &&
			GetTop() == other.GetTop() &&
			GetBottom() == other.GetBottom();
	}
private:
	int left = 0;
	int top = 0;
	int right = 0;
	int bottom = 0;
};