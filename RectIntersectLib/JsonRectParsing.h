#pragma once

#include "RectIntersectLib\Rect.h"

#include <vector>
#include <iosfwd>

// Parses a list of rectangles from a json stream. Throws in case
// any errors are encountered while parsing the json. The json schema
// defining the expected json format can be found in RectangleListSchema.json.
//
// A sample of the expected json input is:
//{
//	"rects": [
//		{ "x": 100, "y" : 100, "w" : 250, "h" : 80 },
//		{ "x": 120, "y" : 200, "w" : 250, "h" : 150 },
//	]
//}
std::vector<Rect> ReadRectsFromJson(std::istream& stream);

// Writes a vector of rectangles to a json stream. Format is
// the same as the function ReadRectsFromJson expects.
void WriteRectsToJson(std::ostream& stream, const std::vector<Rect>& rects);