#pragma once

#include <vector>
#include <functional>

// Utility used for running a function on every subset of a given set.
// All subsets will have elements in the same relative order as in the given set.
void ForEachSubset(const std::vector<int>& set, std::function<void(std::vector<int>)> processingFunction);

// Utility used for counting the number of subsets a set with a given number of elements has.
unsigned long long CountSubsets(size_t setElementCount);