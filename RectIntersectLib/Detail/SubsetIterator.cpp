#include "RectIntersectLib/detail/SubsetIterator.h"

namespace
{
	// Utility struct for storing information for partial subsets, which
	// includes the current contents of the subset, and what was the last
	// element index considered for this subset.
	struct PartialSubset
	{
		PartialSubset(int lastElementReached, std::vector<int> subset)
			: LastElementReached(lastElementReached), Subset(move(subset))
		{}
		unsigned int LastElementReached = 0;
		std::vector<int> Subset;
	};
}

void ForEachSubset(const std::vector<int>& set, std::function<void(std::vector<int>)> processingFunction)
{
	if (set.empty())
	{
		processingFunction(std::vector<int>());
		return;
	}

	// Iteratively build all subsets using a stack. On the stack we keep
	// track of how many elements we processed, and what the current subset is.
	// For every element we duplicate the current subset, and add the element
	// into one of the copies. Once we passed all the elements, we run the
	// processing function on the resulting subset.
	std::vector <PartialSubset> subsetStack;
	// The maximum size this stack is expected to grow is twice the size of the set.
	subsetStack.reserve(set.size() * 2);
	subsetStack.emplace_back(0, std::vector<int>());
	subsetStack.emplace_back(0, std::vector<int>(1, set[0]));

	while (!subsetStack.empty())
	{
		auto currentSubset = std::move(subsetStack.back());
		subsetStack.pop_back();

		auto nextElement = ++currentSubset.LastElementReached;
		if (nextElement == set.size())
		{
			processingFunction(move(currentSubset.Subset));
		}
		else
		{
			subsetStack.emplace_back(nextElement, currentSubset.Subset);
			currentSubset.Subset.emplace_back(set[nextElement]);
			subsetStack.emplace_back(nextElement, std::move(currentSubset.Subset));
		}
	}
}

unsigned long long CountSubsets(size_t setElementCount)
{
	if (setElementCount > sizeof(long long) * 8)
	{
		throw std::exception("Subset count exceeded 2^64.");
	}

	return 1ull << setElementCount;
}