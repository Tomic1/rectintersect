#pragma once

#include "RectIntersectLib/Rect.h"

#include <iosfwd>
#include <vector>

namespace RectIntersect
{
	// Base class for accumulating rectangle intersection results.
	class ResultAccumulatorBase
	{
	public:
		// Resets the state, and gives a set of rectangles whose intersections
		// will be getting accumulated. Takes ownership, so accepting vector by value.
		void Reset(std::vector<Rect> rectangles);

		// Add an intersection with the given indexes. Takes ownership, so accepting
		// the vector by value.
		virtual void AddIntersection(std::vector<int> rectIndexes) = 0;
	protected:
		// Virtual method called from Reset that allows extending classes to do specific
		// to do reset their specific state.
		virtual void DoReset() = 0;

		// Calculates the intersection of a given set of rects. Throws an
		// exception if intersection doesn't exist.
		Rect CalculateIntersection(const std::vector<int>& rectIndexes) const;

		// Getter to the rectangles whose intersections are accumulated.
		const std::vector<Rect>& GetRects() const
		{
			return m_rects;
		}

	private:
		// Rectangles whose intersections will be getting accumulated
		std::vector<Rect> m_rects;
	};

	// Accumulator that streams results as it receives them. Also streams
	// header of rectangles that are getting intersected when it gets reset.
	class StreamingResultAccumulator : public ResultAccumulatorBase
	{
	public:
		StreamingResultAccumulator(std::ostream& stream);

		// Can't copy/assign due to reference member
		StreamingResultAccumulator(const StreamingResultAccumulator&) = delete;
		StreamingResultAccumulator& operator=(const StreamingResultAccumulator&) = delete;

		// Calculates the intersection of the given rects and streams the results.
		virtual void AddIntersection(std::vector<int> rectIndexes) override;

	protected:
		// Streams a header of the rects that were set in this reset.
		virtual void DoReset() override;

		// Writes the rect to the stream.
		void WriteRect(const Rect& rect) const;

	private:
		std::ostream& m_stream;
	};

	// Accumulator that stores all results in a buffer.
	class BufferedResultAccumulator : public ResultAccumulatorBase
	{
	public:
		BufferedResultAccumulator() = default;

		// Disallow copy & assign as the can be slow operations due to potentially 
		// large buffer this class contains.
		BufferedResultAccumulator(const BufferedResultAccumulator&) = delete;
		BufferedResultAccumulator& operator=(const BufferedResultAccumulator&) = delete;

		// Calculates the intersection and adds it to the buffer
		virtual void AddIntersection(std::vector<int> rectIndexes) override;

		// Class that represents an intersection of rectangles.
		struct Intersection
		{
			// Will take ownership of vector, so pass in by value
			Intersection(std::vector<int> rectIndexes, const Rect& intersection);

			// indexes correspond to indexes in the ResultAccumulatorBase::m_rects vector
			std::vector<int> RectIndexes;
			Rect RectIntersection;
		};

		// Getter for the result buffer
		const std::vector<Intersection>& GetIntersections() const;

	protected:
		virtual void DoReset() override;

	private:
		std::vector<Intersection> m_intersections;
	};
}

