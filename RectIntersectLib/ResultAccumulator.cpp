#include "RectIntersectLib\ResultAccumulator.h"

#include <iostream>
#include <vector>

using namespace std;

namespace RectIntersect
{
	void ResultAccumulatorBase::Reset(vector<Rect> rectangles)
	{
		m_rects = move(rectangles);
		DoReset();
	}

	Rect ResultAccumulatorBase::CalculateIntersection(const vector<int>& rectIndexes) const
	{
		if (rectIndexes.size() < 2)
		{
			throw exception("Can't calculate intersection if there is not at least 2 rectangles.");
		}

		Rect result = m_rects[rectIndexes[0]];
		for (size_t i = 1; i < rectIndexes.size(); i++)
		{
			result.IntersectWith(m_rects[rectIndexes[i]]);
		}

		return result;
	}

	StreamingResultAccumulator::StreamingResultAccumulator(ostream& stream)
		: m_stream(stream)
	{
	}

	void StreamingResultAccumulator::WriteRect(const Rect& rect) const
	{
		m_stream << "(" << rect.GetLeft() << ", " << rect.GetTop()
			<< ") w = " << rect.GetWidth() << " h = " << rect.GetHeight() << ".\n";
	}

	void StreamingResultAccumulator::AddIntersection(vector<int> rectIndexes)
	{
		Rect intersection = CalculateIntersection(rectIndexes);

		m_stream << '\t' << "Between rectangle ";
		for (size_t i = 0; i < rectIndexes.size() - 1; i++)
		{
			m_stream << rectIndexes[i] + 1 << ", ";
		}

		m_stream << "and " << rectIndexes.back() + 1 << " at ";

		WriteRect(intersection);
	}

	void StreamingResultAccumulator::DoReset()
	{
		m_stream << "Input:\n";

		for (size_t i = 0; i < GetRects().size(); i++)
		{
			m_stream << '\t' << i + 1 << ": Rectangle at ";
			WriteRect(GetRects()[i]);
		}

		m_stream << "\nIntersections\n";
	}

	BufferedResultAccumulator::Intersection::Intersection(vector<int> rectIndexes, const Rect& intersection)
		: RectIndexes(move(rectIndexes)), RectIntersection(intersection)
	{
	}

	void BufferedResultAccumulator::AddIntersection(vector<int> rectIndexes)
	{
		const Rect intersection = CalculateIntersection(rectIndexes);
		m_intersections.emplace_back(move(rectIndexes), intersection);
	}

	const vector<BufferedResultAccumulator::Intersection>& BufferedResultAccumulator::GetIntersections() const
	{
		return m_intersections;
	}

	void BufferedResultAccumulator::DoReset()
	{
		m_intersections.clear();
	}
}

