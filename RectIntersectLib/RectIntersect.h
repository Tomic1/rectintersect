#pragma once

#include "RectIntersectLib/ResultAccumulator.h"

#include <vector>

namespace RectIntersect
{
	// Gets all intersections of 2 or more rectangles in a given set of rectangles.
	// The resulting intersections will be stored in the given result accumulator.
	// Due to potential for intersection count to be unreasonably large, won't attempt
	// to calculate intersections if there are more than 2^33. In case any error is
	// encountered an exception will be thrown.
	void GetIntersections(const std::vector<Rect>& rects, ResultAccumulatorBase& result);
}