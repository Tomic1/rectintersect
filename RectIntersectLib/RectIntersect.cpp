#include "RectIntersectLib/RectIntersect.h"
#include "RectIntersectLib/Detail/SubsetIterator.h"

#include <unordered_set>
#include <sstream>
#include <algorithm>

using namespace std;
using namespace RectIntersect;

namespace
{
	// Enumeration used to represent the sides of a rectangle
	enum class Side
	{
		Left,
		Top,
		Right,
		Bottom
	};

	// Used to detect rectangle intersections that involve 2 or
	// more rectangles for a given set of rectangles.
	class RectIntersectDetector
	{
	public:
		// Counts the intersections in a given set of rectangles
		// with worst case O(N^2) complexity, which is achieved when
		// all rectangles contain each other. Throws an exception if
		// the number or intersections exceeds 2^64.
		unsigned long long CalculateIntersectionCount(const vector<Rect>& rects)
		{
			m_rects = &rects;
			m_intersectionCount = 0;
			m_mode = Mode::Counting;

			GetAllIntersections();

			return m_intersectionCount;
		}

		// Detects the intersections in a given set of rectangles with worst case
		// O(N^2 + N * I) complexity, where N is the number of input rectangles
		// and I is the number of intersections. This worst-case is achieved when
		// all rectangles contain each other. The detected intersections will be
		// accumulated in the provided result accumulator.
		void GetIntersections(const vector<Rect>& rects, ResultAccumulatorBase& accumulator)
		{
			m_rects = &rects;
			m_accumulator = &accumulator;
			m_mode = Mode::Calculating;

			m_accumulator->Reset(rects);
			GetAllIntersections();
		}

	private:
		// Enumeration representing in which mode the intersection detection is done in.
		enum class Mode
		{
			// When we're only counting the number of intersections, but not calculating them
			Counting,
			// When we're calculating the intersection area for each detected intersection
			Calculating
		};

		// Utility class that represents one side of a rectangle. Includes
		// the originating rectangle index to be able to retrieve it.
		struct RectSide
		{
			// Position argument is either in vertical or horizontal dimension depending
			// on the side. Index corresponds to the originating rectangle's index in the
			// RectIntersectDetector::m_rects vector.
			RectSide(Side side, int position, int index) : Side(side), Position(position), Index(index) {}

			// Two rectangle's sides are considered the same if there originate from the same
			// rectangle and refer to the same side.
			bool operator==(const RectSide& rhs)
			{
				const bool result = Index == rhs.Index && Side == rhs.Side;
#ifdef _DEBUG
				// Sanity check in debug to ensure that we don't have inconsistent data. The
				// position of a given rectangle side shouldn't change.
				if (result && Position != rhs.Position)
				{
					throw exception("Encountered two rect sides belonging to the same rect with different positions");
				}
#endif
				return result;
			};

			bool operator!=(const RectSide& rhs)
			{
				return !this->operator==(rhs);
			};

			Side Side;
			int Position;
			int Index;
		};

		// Gets the points of interest in the vertical sweep order. Points of interest
		// are top and bottom sides of the rectangles in the m_rects vector. We sort
		// them based on the Y coordinate, and want to place bottom sides before
		// top sides in case of them being at the same position.
		vector<RectSide> GetVerticalSweepOrder() const
		{
			vector<RectSide> result;

			// We know the size of the result, so reserve it now
			result.reserve(m_rects->size() * 2);

			for (size_t i = 0; i < m_rects->size(); i++)
			{
				result.emplace_back(Side::Top, m_rects->at(i).GetTop(), static_cast<int>(i));
				result.emplace_back(Side::Bottom, m_rects->at(i).GetBottom(), static_cast<int>(i));
			}

			sort(result.begin(), result.end(), [](const RectSide& lhs, const RectSide& rhs)
			{
				if (lhs.Position != rhs.Position)
				{
					return lhs.Position < rhs.Position;
				}

				// We want to ensure that bottom sides come before top sides, to avoid
				// having additional checks for overlap when the top and bottom sides
				// of two rectangles are at the same position.
				return lhs.Side != rhs.Side ? lhs.Side == Side::Bottom : false;
			});

			return move(result);
		}

		// Takes a given set of rectangle indexes and a new rectangle index, whose
		// rectangles are known to overlap. All combinations of 2 or more rectangles
		// which include the new rectangle are added result accumulator.
		void CalculateIntersections(const vector<int>& rectIndexes, int newIndex)
		{
			ForEachSubset(rectIndexes, [&](vector<int> subset)
			{
				// We don't have an intersection if there's only 1 rectangle
				if (subset.size() == 0)
				{
					return;
				}

				auto insertIndex = lower_bound(subset.begin(), subset.end(), newIndex);
				subset.insert(insertIndex, newIndex);

				m_accumulator->AddIntersection(move(subset));
			});
		}

		// Takes a given set of rectangle indexes and two new rectangle indexes, whose
		// rectangles are known to overlap. All combinations of 2 or more rectangles
		// which include both new rectangle are added result accumulator.
		void CalculateIntersections(const vector<int>& rectIndexes, int newIndex1, int newIndex2)
		{
			ForEachSubset(rectIndexes, [&](vector<int> subset)
			{
				auto insertIndex = lower_bound(subset.begin(), subset.end(), newIndex1);
				subset.insert(insertIndex, newIndex1);

				insertIndex = lower_bound(subset.begin(), subset.end(), newIndex2);
				subset.insert(insertIndex, newIndex2);

				m_accumulator->AddIntersection(move(subset));
			});
		}

		// Gets all the intersections of a given rect with the rects in the given sweep line.
		// At this point in the algorithm we know that all the given rects have vertical overlap
		// at the horizontalSweepPosition sweep line position. To get the rectangle intersections we do a
		// horizontal sweep, where points of interest are left/right rectangle sides, and keep
		// track of the overlapping rectangles. When we reach the left side of the given rect,
		// we calculate all intersections with the horizontalSweepPosition set of overlapping rectangles. We then
		// continue the horizontal sweep until we reach the end of the given rectangle, maintaining the
		// set of overlapping rectangles. For every additional left side we encounter we calculate 
		// all intersections with the horizontalSweepPosition set of overlapping rectangles.
		void GetIntersections(vector<RectSide>& sweepLineOverlap, int rectIndex)
		{
			const vector<Rect>& rects = *m_rects;
			const Rect& target = rects[rectIndex];
			// Represents the indexes of rectangles that overlap at the horizontalSweepPosition
			// position of the horizontal sweep line. Is maintained to be sorted
			// for testability.
			vector<int> horizontalSweepLine;

			auto addToHorizontalSweepLine = [&](int rectIndex)
			{
				auto insertIndex = lower_bound(horizontalSweepLine.begin(), horizontalSweepLine.end(), rectIndex);
				horizontalSweepLine.insert(insertIndex, rectIndex);
			};

			auto removeFromHorizontalSweepLine = [&](int rectIndex)
			{
				auto removeIndex = lower_bound(horizontalSweepLine.begin(), horizontalSweepLine.end(), rectIndex);
#ifdef _DEBUG
				if (removeIndex == horizontalSweepLine.end() || *removeIndex != rectIndex)
				{
					throw exception("Invalid state encountered in intersecting index set");
				}
#endif
				horizontalSweepLine.erase(removeIndex);
			};

			auto horizontalSweepPosition = sweepLineOverlap.begin();

			// We sweep through the overlapping rects at the current position of the vertical sweep line
			// until we reach a side that comes after the target rectangle's left side.
			while (horizontalSweepPosition != sweepLineOverlap.end() && horizontalSweepPosition->Position <= target.GetLeft())
			{
				switch (horizontalSweepPosition->Side)
				{
				case Side::Left:
					addToHorizontalSweepLine(horizontalSweepPosition->Index);
					break;
				case Side::Right:
					removeFromHorizontalSweepLine(horizontalSweepPosition->Index);
					break;
				default:
					throw exception("Unexpected side encountered in sweep line.");
				}
				horizontalSweepPosition++;
			}

			// At this point we know that the target rectangle intersects all of the rectangles
			// that are overlapping at the current horizontal index.
			switch (m_mode)
			{
			case Mode::Counting:
				// We know the number of intersections is the same as the number of subsets
				// of the current overlapping rectangles, as each subset + the new rectangle
				// is a unique set of intersecting rectangles. We exclude the empty subset
				// as we need 2 or more rectangles to have an intersection.
				m_intersectionCount += CountSubsets(horizontalSweepLine.size()) - 1;
				break;
			case Mode::Calculating:
				CalculateIntersections(horizontalSweepLine, rectIndex);
				break;
			default:
				throw exception("Unexpected mode encountered");
			}

			// We continue sweeping until we reach the target rectangle's right side to add
			// the intersections with any additional overlapping rectangle we encounter.
			while (horizontalSweepPosition != sweepLineOverlap.end() && horizontalSweepPosition->Position < target.GetRight())
			{
				switch (horizontalSweepPosition->Side)
				{
				case Side::Left:
					// Since we didn't reach the right side of the target rectangle, we know
					// it intersects with the rectangle at the current position.
					switch (m_mode)
					{
					case Mode::Counting:
						// In this case we're counting all subsets + new rect + rect at current index.
						// Because we add 2 elements to each subset we don't decrease the count by 1.
						m_intersectionCount += CountSubsets(horizontalSweepLine.size());
						break;
					case Mode::Calculating:
						// We want to add only combinations or rectangles that include both the target
						// rect and the rect at the current position, to avoid duplicates.
						CalculateIntersections(horizontalSweepLine, rectIndex, horizontalSweepPosition->Index);
						break;
					default:
						throw exception("Unexpected mode encountered");
					}
					addToHorizontalSweepLine(horizontalSweepPosition->Index);
					break;
				case Side::Right:
					removeFromHorizontalSweepLine(horizontalSweepPosition->Index);
					break;
				default:
					throw exception("Unexpected side encountered in sweep line.");
				}
				horizontalSweepPosition++;
			}
		}

		// Determines in what order the horizontal sweep will encounter rectangles.
		static bool HorizontalSweepLineOrder(const RectSide& lhs, const RectSide& rhs)
		{
			if (lhs.Position != rhs.Position)
			{
				return lhs.Position < rhs.Position;
			}

			// We want to ensure that right sides come before left sides, to avoid
			// having additional checks for overlap when the left and right sides
			// of two rectangles are at the same position.
			return lhs.Side != rhs.Side ? lhs.Side == Side::Right : false;
		}

		void RemoveRectSideFromVerticalSweepLineOverlap(vector<RectSide>& sweepLineOverlap, const RectSide& side) const
		{
			// The sides in the vertical overlap are sorted, so we can use binary search to find it
			auto findIndex = lower_bound(sweepLineOverlap.begin(), sweepLineOverlap.end(), side, HorizontalSweepLineOrder);
			// Since there could be multiple rectangle sides that end at the same position
			// we need to search sequentially from there until we find the one we're removing.
			while (findIndex != sweepLineOverlap.end() && *findIndex != side)
			{
				findIndex++;
			}

#ifdef _DEBUG
			// Sanity check that we found the sweep line.
			if (findIndex == sweepLineOverlap.end())
			{
				throw exception("Failed to find expected rect part in sweep line");
			}
#endif

			sweepLineOverlap.erase(findIndex);
		}

		void RemoveRectFromVerticalSweepLineOverlap(vector<RectSide>& sweepLineOverlap, int rectIndex) const
		{
			const RectSide left(Side::Left, m_rects->at(rectIndex).GetLeft(), rectIndex);
			RemoveRectSideFromVerticalSweepLineOverlap(sweepLineOverlap, left);

			const RectSide right(Side::Right, m_rects->at(rectIndex).GetRight(), rectIndex);
			RemoveRectSideFromVerticalSweepLineOverlap(sweepLineOverlap, right);
		}

		// Add the rectangle at the given index to the sweep line overlap set. This
		// set is kept sorted in the horizontal sweep order to allow horizontal sweeps.
		void AddRectToVerticalSweepLineOverlap(vector<RectSide>& sweepLineOverlap, int rectIndex) const
		{
			RectSide left(Side::Left, m_rects->at(rectIndex).GetLeft(), rectIndex);
			auto findIndex = lower_bound(sweepLineOverlap.begin(), sweepLineOverlap.end(), left, HorizontalSweepLineOrder);
			sweepLineOverlap.insert(findIndex, left);

			RectSide right(Side::Right, m_rects->at(rectIndex).GetRight(), rectIndex);
			findIndex = lower_bound(sweepLineOverlap.begin(), sweepLineOverlap.end(), right, HorizontalSweepLineOrder);
			sweepLineOverlap.insert(findIndex, right);
		}

		// Intersections are detected using a sweep line algorithm that sweeps
		// through the space vertically, where points of interest are the top
		// and bottom sides of each rect. When we encounter a rectangle top we
		// traverse the rectangles that are currently intersecting the sweep line
		// and detect all intersections (based on the rectangle's left and right 
		// sides), after which we add the rectangle to the sweep line overlap set.
		// When we come across the bottom of a rect, we remove it from the sweep
		// line overlap.
		void GetAllIntersections()
		{
			vector<RectSide> sweepOrder = GetVerticalSweepOrder();
			// The vertical sweep line overlap is kept sorted in the order
			// the horizontal sweep will be done.
			vector<RectSide> verticalSweepLineOverlap;

			for (auto& rectSide : sweepOrder)
			{
				switch (rectSide.Side)
				{
				case Side::Top:
					GetIntersections(verticalSweepLineOverlap, rectSide.Index);
					AddRectToVerticalSweepLineOverlap(verticalSweepLineOverlap, rectSide.Index);
					break;
				case Side::Bottom:
					RemoveRectFromVerticalSweepLineOverlap(verticalSweepLineOverlap, rectSide.Index);
					break;
				default:
					throw exception();
				}
			}
		}

		// Rectangles which are currently being processed.
		const vector<Rect>* m_rects = nullptr;

		// Accumulator used for storing detected intersections. Is expected
		// to be used only when the current mode is "Calculating"
		ResultAccumulatorBase* m_accumulator = nullptr;

		// Intersection counter. Is expected to be used only when
		// the current mode is "Counting"
		unsigned long long m_intersectionCount = 0;

		// Current mode the detector is in.
		Mode m_mode = Mode::Calculating;
	};
}

namespace RectIntersect
{
	void GetIntersections(const vector<Rect>& rects, ResultAccumulatorBase& result)
	{
		RectIntersectDetector detector;
		// We don't support input where the size of the output would
		// require an unreasonable amount of time to calculate.
		constexpr unsigned long long maxIntersectCount = 1ull << 33;

		unsigned long long intersectionCount = detector.CalculateIntersectionCount(rects);
		if (intersectionCount > maxIntersectCount)
		{
			stringstream error;
			error << "Encountered " << intersectionCount << " intersections, which is more than the maximum " << maxIntersectCount << " intersections.";
			throw exception(error.str().c_str());
		}

		detector.GetIntersections(rects, result);
	}
}